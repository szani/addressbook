<head>
	<meta charset="utf-8">
	<!-- Foundation -->
	<link rel="stylesheet" href="css/foundation.css" />
    <link rel="stylesheet" href="css/custom.css" />
    <script src="js/vendor/modernizr.js"></script>
    <script src="js/foundation/foundation.js"></script>
    <script src="js/foundation/foundation.abide.js"></script>
	<!-- Tablesorter: required -->
	<link rel="plugins/tablesorter/stylesheet" href="plugins/tablesorter/css/theme.blue.css">
	<script src="plugins/tablesorter/js/jquery.tablesorter.js"></script>
	<!--pager-->
	<link rel="stylesheet" href="plugins/tablesorter/addons/pager/jquery.tablesorter.pager.css">
	<script src="plugins/tablesorter/addons/pager/jquery.tablesorter.pager.js"></script>
	<!--Fast select-->
	<link rel="stylesheet" href="plugins/fastselect/dist/fastselect.min.css">
	<!-- Multiselect -->
    <link href="plugins/lou-multi-select/css/multi-select.css" media="screen" rel="stylesheet" type="text/css"/>
    

	<!--Sorbarendezés ajax-val -->

	<script id="js">$(function() {

	$("table").tablesorter({ theme : 'blue' });

	$("#ajax-append").click(function() {

		$.get("assets/ajax-content.html", function(html) {

			// append the "ajax'd" data to the table body
			$("table tbody").append(html);

			// let the plugin know that we made a update
			// the resort flag set to anything BUT false (no quotes) will trigger an automatic
			// table resort using the current sort
			var resort = true;
			$("table").trigger("update", [resort]);

			// triggering the "update" function will resort the table using the current sort; since version 2.0.14
			// use the following code to change the sort; set sorting column and direction, this will sort on the first and third column
			// var sorting = [[2,1],[0,0]];
			// $("table").trigger("sorton", [sorting]);
		});

		return false;
	});

	});
	</script>
	
	<!--Lapozás Ajax-val -->
	
	<style>
		
				/* pager wrapper, div */
		.pager {
		  padding: 5px;
		}
		/* pager wrapper, in thead/tfoot */
		td.pager {
		  background-color: #e6eeee;
		}
		/* pager navigation arrows */
		.pager img {
		  vertical-align: middle;
		  margin-right: 2px;
		}
		/* pager output text */
		.pager .pagedisplay {
		  font-size: 11px;
		  padding: 0 5px 0 5px;
		  width: 50px;
		  text-align: center;
		}
		
		/*** loading ajax indeterminate progress indicator ***/
		#tablesorterPagerLoading {
		  background: rgba(255,255,255,0.8) url(icons/loading.gif) center center no-repeat;
		  position: absolute;
		  z-index: 1000;
		}
		
		/*** css used when "updateArrows" option is true ***/
		/* the pager itself gets a disabled class when the number of rows is less than the size */
		.pager.disabled {
		  display: none;
		}
		/* hide or fade out pager arrows when the first or last row is visible */
		.pager img.disabled {
		  /* visibility: hidden */
		  opacity: 0.5;
		  filter: alpha(opacity=50);
		}
	</style>
	
	
	
</head>
<?php include ('core/init.php'); 
require_once('helpers/category_helper.php');
?>

<?php
//Create DB Object
$db = new Database;

//Run Query 
$db->query("SELECT * FROM `addressbook_contacts`");

//Assign Result Set
$contacts = $db->resultset();
?>

<div class="row" id="main">
	<div class="large-12 columns">
		<div id="demo"><table class="tablesorter">
			<thead>
				<tr class="tablesorter-ignoreRow">
					<td class="pager" colspan="6">
					    <img src="plugins/tablesorter/addons/pager/icons/first.png" class="first"/>
					    <img src="plugins/tablesorter/addons/pager/icons/prev.png" class="prev"/>
					    <span class="pagedisplay"></span> <!-- this can be any element, including an input -->
					    <img src="plugins/tablesorter/addons/pager/icons/next.png" class="next"/>
					    <img src="plugins/tablesorter/addons/pager/icons/last.png" class="last"/>
					</td>
				</tr>
				<tr>
					<th width="200">Név </th>
					<th width="130">Telefonszám</th>
					<th width="200">Email</th>
					<th width="250">Cím</th>
					<th width="100">Kategória</th>
					<th width="150">Szerkesztés</th>
				</tr>
			</thead>
				<tfoot><!-- tfoot text will be updated at the same time as the thead -->
				    <tr><th>Név</th><th>Telefonszám</th><th>Email</th><th>Cím</th><th>Kategória</th><th>Szerkesztés</th></tr>
				    <tr>
				      <td class="pager" colspan="6">
				        <img src="plugins/tablesorter/addons/pager/icons/first.png" class="first"/>
				        <img src="plugins/tablesorter/addons/pager/icons/prev.png" class="prev"/>
				        <span class="pagedisplay"></span> <!-- this can be any element, including an input -->
				        <img src="plugins/tablesorter/addons/pager/icons/next.png" class="next"/>
				        <img src="plugins/tablesorter/addons/pager/icons/last.png" class="last"/>
				      </td>
				    </tr>
				</tfoot>
				<tbody>
				    <?php foreach($contacts as $contact) : ?>
					<tr>
						<td><a href="contact.html"><?php echo $contact->first_name.' '.$contact->last_name; ?></a></td>
						<td><?php if($contact->phone) echo $contact->phone; ?></td>
						<td><?php echo $contact->email; ?></td>
						<td>
						<ul>
							<li><?php echo $contact->address1; ?></li>
							<li><?php if($contact->address2) echo $contact->address2; ?></li>
							<li><?php echo $contact->city; ?> <?php echo $contact->zipcode; ?></li>
						</ul>
						</td>
						<td> <?php 
						           $value = $contact->contact_group;
						           $string= 'Besorolatlan';
						           if (empty($value)) {
						               echo $string;
						               }
						           else echo $value;
						     ?>
						</td> 
                   	<td>
						<ul class="button-group">
								<li> <!--a linkre katttintva megnyillik az editcontact form -->
                            	<a href="#" class="button tiny" data-reveal-id="editModal<?php echo $contact->id;?>" data-contact-id="<?php echo $contact->id;?>">Szerkeszt</a>
                            	<div id="editModal<?php echo $contact->id;?>" data-cid="<?php echo $contact->id; ?>" class="reveal-modal editModal" data-reveal>
                            		<h2>Kapcsolat szerkesztése</h2> <!-- kapcsolati elemek neve ugyanaz mint az adatbázisban -->
					                <form id="editContact" action="#" method="post">
					                	 <div class="row">
					                        <div class="large-6 columns" class="name-field">
					                           <label>Vezetéknév<small>required</small>
					                                	<input name="first_name" required type="text" required <?php preg_match("/^[a-zA-ZáéíóöőúüűÁÉÍÓÖŐÚÜŰ\s]*$/u") ?> placeholder="Írja be a vezetéknevét!" value="<?php echo $contact->first_name; ?>" />
					                            </label>
					                            <small class="error">A név csak betűből állhat!</small>
					                        </div>
					                       	<div class="large-6 columns" class="name-field">
												  <label>Keresztnév<small>required</small>
					                               	<input name="last_name" required type="text" required <?php preg_match("/^[a-zA-ZáéíóöőúüűÁÉÍÓÖŐÚÜŰ\s]*$/u") ?> placeholder="Írja be a keresztnevét!" value="<?php echo $contact->last_name; ?>"/>
												  </label>
												  <small class="error">A név csak betűből állhat!</small>
					                        </div>
					                    </div>
					                	 <div class="row">
					                        <div class="large-6 columns" class="email-field">
											    <label>Email<small>required</small>
					                               	<input name="email" type="email" required placeholder="Írja be az email címet!" value="<?php echo $contact->email; ?>"/>
												</label>
												<small class="error">Kötelező érvényes email cím!</small>
					                        </div>
					                        <div class="large-6 columns">
					                            <label>Telefonszám
					    							<input name="phone" type="tel" pattern="[\+]\d{2}[\(]\d{1,2}[\)]\d{3,4}[\-]\d{3}"  title='Telefonszám (minta: +99(99)9999-999)'  placeholder="+99(99)9999-999" value="<?php echo $contact->phone; ?>"/>
					    						</label>
					                        </div>
					                    </div>
					                	    <div class="row">
						                        <div class="large-6 columns">
						                            <label>Irányítószám
															<input name="zipcode" type="number" type="quantity" min="1000" max="9999" placeholder ="Írja be a négyszámjegyű irányítószámot!" value="<?php echo $contact->zipcode; ?>"/>
													  </label>
						                        </div>
						                        <div class="large-6 columns">
						                            <label>Település
														<input name="city" type="text" placeholder="Írja be a várost!" value="<?php echo $contact->city; ?>"/>
													</label>
						                        </div>
						                    </div>
						                    <div class="row">
						                        <div class="large-6 columns">
						                            <label>Cím 1
						        						<input name="address1" type="text" placeholder="Írja be az 1. címet!" value="<?php echo $contact->address1; ?>"/>
						        					</label>
						                        </div>
						                        <div class="large-6 columns">
						                            <label>Cím 2
						        						<input name="address2" type="text" placeholder="Írja be a 2. címet!" value="<?php echo $contact->address2; ?>"/>
						        					</label>
						                        </div>
						                    </div>
						                    <div class="row">
						                        <div class="large-6 columns"> 
						                            <label> Kategória
						                            </label >
						                            <input 
						                                name="contact_group[]"
						                                type="text"
						                                multiple
						                                class="tagsInput"
						                                multiple class="multipleInputDynamic"
						                                data-user-option-allowed="true"
						                                data-url="plugins/fastselect/data.json"
						                                data-load-once="true"
						                                placeholder="+ Új kategória"
						                                value='<?php 
														           $value = $contact->contact_group;
														           $string= 'Besorolatlan';
														           if (empty($value)) {
														               echo $string;
														               }
														           else echo $value;
														        ?>'
						                                data-initial-value='<?php 
																	           $value = $contact->contact_group;
																	           $string= 'Besorolatlan';
																	           if (empty($value)) {
																	               echo $string;
																	               }
																	           else echo $value;
																	        ?>'
						                            />
						                        </div>
						                        <div class="large-12 columns">
						        					<label>Megjegyzések
						        						<textarea name="notes" placeholder="Megjegyzések"><?php echo $contact->notes; ?></textarea>
						        					</label>
						        				</div>
						                    </div>
									<input type="hidden" name="id" value="<?php echo $contact->id; ?>"/>
					                <input name="submit" type="submit" class="add-btn button right" value="Frissít" />
					                <a class="close-reveal-modal">&#215;</a>
					                </form>
                            	</div>
                            </li>
                            <li>
                            	<form id="deleteContact" action="#" method="post" >
                            		<input type="hidden" name="id" value="<?php echo $contact->id; ?>" />
                            		<input type="submit" class="delete-btn button tiny secondary alert" value="Töröl" />
                            	</form>
                            </li>
                        </ul>
                    </td>
                </tr>
                <?php endforeach ; ?>
            </tbody>
        </table>
     	</div>
    </div>
</div>
	<script>
		
	$(function(){
	
	  var $table = $('table'),
	  // define pager options
	  pagerOptions = {
	    // target the pager markup - see the HTML block below
	    container: $(".pager"),
	    // output string - default is '{page}/{totalPages}';
	    // possible variables: {size}, {page}, {totalPages}, {filteredPages}, {startRow}, {endRow}, {filteredRows} and {totalRows}
	    // also {page:input} & {startRow:input} will add a modifiable input in place of the value
	    output: '{startRow} - {endRow} / {filteredRows} ({totalRows})',
	    // if true, the table will remain the same height no matter how many records are displayed. The space is made up by an empty
	    // table row set to a height to compensate; default is false
	    fixedHeight: true,
	    // remove rows from the table to speed up the sort of large tables.
	    // setting this to false, only hides the non-visible rows; needed if you plan to add/remove rows with the pager enabled.
	    removeRows: false,
	    // go to page selector - select dropdown that sets the current page
	    cssGoto: '.gotoPage'
	  };
	
	  // Initialize tablesorter
	  // ***********************
	  $table
	    .tablesorter({
	      theme: 'blue',
	      headerTemplate : '{content} {icon}', // new in v2.7. Needed to add the bootstrap icon!
	      widthFixed: true,
	      widgets: ['zebra', 'filter']
	    })
	
	    // initialize the pager plugin
	    // ****************************
	    .tablesorterPager(pagerOptions);
	
	    // Add two new rows using the "addRows" method
	    // the "update" method doesn't work here because not all rows are
	    // present in the table when the pager is applied ("removeRows" is false)
	    // ***********************************************************************
	    var r, $row, num = 50,
	      row = '<tr><td>Student{i}</td><td>{m}</td><td>{g}</td><td>{r}</td><td>{r}</td><td>{r}</td><td>{r}</td><td><button type="button" class="remove" title="Remove this row">X</button></td></tr>' +
	        '<tr><td>Student{j}</td><td>{m}</td><td>{g}</td><td>{r}</td><td>{r}</td><td>{r}</td><td>{r}</td><td><button type="button" class="remove" title="Remove this row">X</button></td></tr>';
	    $('button:contains(Add)').click(function(){
	      // add two rows of random data!
	      r = row.replace(/\{[gijmr]\}/g, function(m){
	        return {
	          '{i}' : num + 1,
	          '{j}' : num + 2,
	          '{r}' : Math.round(Math.random() * 100),
	          '{g}' : Math.random() > 0.5 ? 'male' : 'female',
	          '{m}' : Math.random() > 0.5 ? 'Mathematics' : 'Languages'
	        }[m];
	      });
	      num = num + 2;
	      $row = $(r);
	      $table
	        .find('tbody').append($row)
	        .trigger('addRows', [$row]);
	      return false;
	    });
	
	    // Delete a row
	    // *************
	    $table.delegate('button.remove', 'click' ,function(){
	      // NOTE this special treatment is only needed if `removeRows` is `true`
	      // disabling the pager will restore all table rows
	      $table.trigger('disablePager');
	      // remove chosen row
	      $(this).closest('tr').remove();
	      // restore pager
	      $table.trigger('enablePager');
	    });
	
	    // Destroy pager / Restore pager
	    // **************
	    $('button:contains(Destroy)').click(function(){
	      // Exterminate, annhilate, destroy! http://www.youtube.com/watch?v=LOqn8FxuyFs
	      var $t = $(this);
	      if (/Destroy/.test( $t.text() )){
	        $table.trigger('destroyPager');
	        $t.text('Restore Pager');
	      } else {
	        $table.tablesorterPager(pagerOptions);
	        $t.text('Destroy Pager');
	      }
	      return false;
	    });
	
	    // Disable / Enable
	    // **************
	    $('.toggle').click(function(){
	      var mode = /Disable/.test( $(this).text() );
	      // triggering disablePager or enablePager
	      $table.trigger( (mode ? 'disable' : 'enable') + 'Pager');
	      $(this).text( ( mode ? 'Enable' : 'Disable' ) + ' Pager');
	      return false;
	    });
	    $table.bind('pagerChange', function(){
	      // pager automatically enables when table is sorted.
	      $('.toggle').text( 'Disable Pager' );
	    });
	
	});
	</script>

 <!-- Kép betöltése -->
        <div id="loaderImage">
            <img src="img/ajax-loader.gif" />
        </div>
    <!-- Fő Contetnt -->
        <div id="pageContent"></div>
    <!-- /Fő Content -->
    <script>
      $(document).foundation();
      // $(document).foundation('abide', 'reflow');
    </script>
    <script>
      $('.public-methods').multiSelect();
      $('#your-select').multiSelect({});
      $('.tagsInput').fastselect();
    </script>