<!doctype html>
<html  lang="hun">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Address Book | Címjegyzék</title>
    <link rel="stylesheet" href="css/foundation.css" />
    <link rel="stylesheet" href="css/custom.css" />
    <link href="plugins/lou-multi-select/css/multi-select.css" media="screen" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="plugins/fastselect/dist/fastselect.min.css">
    <script src="js/vendor/modernizr.js"></script>
    <script src="js/foundation/foundation.js"></script>
    <script src="js/foundation/foundation.abide.js"></script>
    <style type="text/css">
        body {
          font-size: 14px;
          font-family: Arial, Helvetica, sans-serif;
          margin: 20px;
        }
    </style>
  </head>
  <body>
    <div class="row">
        <div class="large-6 columns">
            <h1>Címjegyzék</h1>
        </div>
        <div class="large-6 columns">
            <a class="add-btn button right small" data-reveal-id="addModal">Kapcsolat hozzáadása</a>
            <div id="addModal" class="reveal-modal" data-reveal>
                <h2>Kapcsolat hozzáadása</h2>
                <!-- kapcsolati elemek neve ugyanaz mint az adatbázisban -->
                <form id="addContact" action="#" method="post" name="addContact">
                    <div class="row">
                        <div class="large-6 columns" class="name-field">
                           <label>Vezetéknév<small>required</small>
                                	<input name="first_name" required type="text" required <?php preg_match("/^[a-zA-ZáéíóöőúüűÁÉÍÓÖŐÚÜŰ\s]*$/u") ?> placeholder="Írja be a vezetéknevét!" />
                            </label>
                            <small class="error">A név csak betűből állhat!</small>
                        </div>
                       	<div class="large-6 columns" class="name-field">
							  <label>Keresztnév<small>required</small>
                               	<input name="last_name" required type="text" required <?php preg_match("/^[a-zA-ZáéíóöőúüűÁÉÍÓÖŐÚÜŰ\s]*$/u") ?> placeholder="Írja be a keresztnevét!" />
							  </label>
							  <small class="error">A név csak betűből állhat!</small>
                        </div>
                    </div>
                    <div class="row">
                        <div class="large-6 columns" class="email-field">
						    <label>Email<small>required</small>
                               	<input name="email" type="email" required placeholder="Írja be az email címet!" />
							</label>
							<small class="error">Kötelező érvényes email cím!</small>
                        </div>
                        <div class="large-6 columns">
                            <label>Telefonszám
    							<input name="phone" type="tel" pattern="[\+]\d{2}[\(]\d{1,2}[\)]\d{3,4}[\-]\d{3}"  title='Telefonszám (minta: +99(99)9999-999)'  placeholder="+99(99)9999-999" />
    						</label>
                        </div>
                      
                    </div>
                    <div class="row">
                        <div class="large-6 columns">
                            <label>Irányítószám
									<input name="zipcode" type="number" type="quantity" min="1000" max="9999" placeholder ="Írja be a négyszámjegyű irányítószámot!"/>
							  </label>
                        </div>
                        <div class="large-6 columns">
                            <label>Település
								<input name="city" type="text" placeholder="Írja be a várost!" />
							</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="large-6 columns">
                            <label>Cím 1
        						<input name="address1" type="text" placeholder="Írja be az 1. címet!" />
        					</label>
                        </div>
                        <div class="large-6 columns">
                            <label>Cím 2
        						<input name="address2" type="text" placeholder="Írja be a 2. címet!" />
        					</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="large-6 columns"> 
                            <label> Kategória
                            </label >
                            <input 
                                name="contact_group[]"
                                type="text"
                                multiple
                                required= "true"
                                class="tagsInput"
                                multiple class="multipleInputDynamic"
                                data-user-option-allowed="true"
                                data-url="plugins/fastselect/data.json"
                                data-load-once="true"
                                placeholder="+ Új kategória"
                            />
                            <small class="error">Legalább egy elem kötelező!</small>
                        </div>
                        <div class="large-12 columns">
        					<label>Megjegyzések
        						<textarea name="notes" placeholder="Megjegyzések"></textarea>
        					</label>
        				</div>
                    </div>
                        <input name="submit" type="submit" class="add-btn button right small" value="Hozzáad" />
                        <a class="close-reveal-modal">&#215;</a>
                </form>
            </div>
        </div>
    </div>
    <!-- Kép betöltése -->
    
        <div id="loaderImage">
            <img src="img/ajax-loader.gif" />
        </div>
    <!-- Fő Contetnt -->
        <div id="pageContent"></div>
    <!-- /Fő Content -->
    <script src="js/vendor/jquery.js"></script>
	<script src="js/script.js"></script>
	<script src="js/foundation.min.js"></script>
	<script src="js/foundation/foundation.abide.js"></script>
	<script src="plugins/lou-multi-select/js/jquery.multi-select.js" type="text/javascript"></script>
	<script src="plugins/fastselect/dist/fastselect.standalone.js"></script>
    <script>
      $(document).foundation();
      // $(document).foundation('abide', 'reflow');
    </script>
    <script>
      $('.public-methods').multiSelect();
      $('#your-select').multiSelect({});
      $('.tagsInput').fastselect();
  //    document.getElementsByClassName('fstChoiceRemove')[0].style.visibility='hidden';
     
    </script>
  </body>
</html>
