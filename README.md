# Address Book README

Fejlesztői környezet: 

php 7.0.25, MySQL adatbázis, 
szerver verzió: 5.5.57 (db name: addressbook),
Apache/2.4.7, 
phpMyAdmin: 4.0.10deb1, 
online hozzáférés: https://stylehub-stabbia.c9users.io/addressbook",

Felhasznált technológiák:

Composer,
Zurb Foundation HTML5 keretrendrendszer,
PDO PHP Objektum,
Ajax,
Javascript,
jQuery,
json,
Git

Pluginok: 

Fastselect: http://dbrekalo.github.io/fastselect/#section-about,
Lou-Moulti-Select: https://github.com/lou/multi-select,
Tableshorter: https://mottie.github.io/tablesorter/

Felhasznált szakirodalom:

PHP Project  - Address Book (Tutorial Videos), All Videos Rights and Description (@Udemy University)

