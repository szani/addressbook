$(document).ready(function(){
    
    //Megmutatjuk a betöltő képet
    $('#loaderImage').show();

    
    //Oldalbetöltésnél megmutatja a kapcsolatokat
       
    showContacts();

    //Kapcsolat hozzáadása - Method 1 
    
    $(document).on('submit','#addContact',function(){
        //Megmutatjuk a betöltő képet
        $('#loaderImage').show();
          
        //post adat a formból
        $.post("add_contact.php", $(this).serialize())
            .done(function(data){
                console.log(data);
                $('#addModal').foundation('reveal','close');
                showContacts();
            });
            return false;
    });
    
  /*   //Kapcsolat hozzáadása - Method 2 
    
    $(document).on('submit','#addContact',function(){
        //Megmutatjuk a betöltő képet
        $('#loaderImage').show();
        
        var inputs = $('#addContact').serialize();
          
        //post adat a formból
        $.ajax({
            type: 'POST',
            url: 'add_contact.php',
            data: inputs,
            success: function(){
            $('#addModal').foundation('reveal','close');
            showContacts();
            }
        })
    }); */
    
    //Kapcsolat szereksztése
    
     $(document).on('submit','#editContact',function(){
        //Megmutatjuk a betöltő képet
        $('#loaderImage').show();
          
        //post adat a formból
        $.post("edit_contact.php", $(this).serialize())
            .done(function(data){
                console.log(data);
                $('#editdModal').foundation('reveal','close');
                showContacts();
            });
            return false;
    });
    
    //Kapcsolat törlése 
    
     $(document).on('submit','#deleteContact',function(){
        //Megmutatjuk a betöltő képet
        $('#loaderImage').show();
          
        //post adat a formból
        $.post("delete_contact.php", $(this).serialize())
            .done(function(data){
                console.log(data);
                showContacts();
            });
            return false;
    });
    
    
});

//Tableshorter


  
// Kapcsolatok mutatása
function showContacts(){
    setTimeout("$('#pageContent').load('contacts.php',function(){$('loaderImage').hide();})",1000);
}

//Kapcsolat zárása

  $(document).on('click','.close-reveal-model',function(){
        $('.reveal-model').foundation('reveal','close');
});

