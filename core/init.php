<?php
// include Config
require_once('config/config.php');

//
require_once('libraries/Database.php');

//Helper Files
require_once('helpers/system_helper.php');

//Autoload MÁGIKUS METÓDUS. A függvény akkor hívódik meg, mikor futásidőben olyan osztályt, interfészt használtunk fel, amit előzőleg nem deklaráltunk.
function __autoload($class_name) {
    require_once('libraries/'.$class_name.'.php');
};