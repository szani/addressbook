<?php 
 header("Content-Type: text/html; charset='utf-8'");
 include ('core/init.php'); 
 ?>

<?php
    //Készítünk egy DB objektumot
    
    $db = new Database();
    
   //Query futtatása
    
    $db->query('INSERT INTO `addressbook_contacts` (first_name, last_name, email, phone, address1, address2, city, zipcode, contact_group, notes)
                    VALUES (:first_name, :last_name, :email, :phone, :address1, :address2, :city, :zipcode, :contact_group, :notes)');
   
   //Adatkötés
 
   $db->bind(':first_name', $_POST['first_name']);
   $db->bind(':last_name', $_POST['last_name']);
   $db->bind(':email', $_POST['email']);
   $db->bind(':phone', $_POST['phone']);
   $db->bind(':address1', $_POST['address1']);
   $db->bind(':address2', $_POST['address2']);
   $db->bind(':city', $_POST['city']);
   $db->bind(':zipcode', $_POST['zipcode']);
   $db->bind(':contact_group',  implode(" ", $_POST['contact_group']));
   $db->bind(':notes', $_POST['notes']);

    if($db->execute()){
        echo "Kapcsolat hozzáadva";
    } else {
        echo "Kapcsolat hozzáadása nem lehetséges";
    }
    
?>

